package services

import (
	context "context"
	"server/resource"
)

type protobufServer struct {
}

func NewProtobufServer() ProtobufServer {
	return protobufServer{}
}

func (protobufServer) mustEmbedUnimplementedProtobufServer() {}

func (protobufServer) FindMaxSumProto(ctx context.Context, req *FindMaxSumRequest) (*FindMaxSumResponse, error) {

	data := resource.FindMaxSum()

	res := FindMaxSumResponse{
		Output: int64(data),
	}

	return &res, nil

}

func (protobufServer) ConvertLRString(ctx context.Context, req *ConvertLRStringRequest) (*ConvertLRStringResponse, error) {

	data := resource.ConvertLRString(req.Input)

	res := ConvertLRStringResponse{
		Output: data,
	}

	return &res, nil

}
func (protobufServer) ContWords(context.Context, *ContWordsRequest) (*ContWordsResponse, error) {

	contWordsRes, err := resource.ContWords()
	if err != nil {
		return nil, err
	}

	res := ContWordsResponse{
		Beef: &Beef{
			TBone:    contWordsRes.TBone,
			Fatback:  contWordsRes.Fatback,
			Pastrami: contWordsRes.Pastrami,
			Pork:     contWordsRes.Pork,
			Meatloaf: contWordsRes.Meatloaf,
			Jowl:     contWordsRes.Jowl,
			Enim:     contWordsRes.Enim,
			Bresaola: contWordsRes.Bresaola,
		},
	}

	return &res, nil
}
