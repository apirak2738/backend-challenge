package resource

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"server/utility"
	"strings"
)

type BeefModels struct {
	TBone    int64 `json:"t-bone"`
	Fatback  int64 `json:"fatback"`
	Pastrami int64 `json:"pastrami"`
	Pork     int64 `json:"pork"`
	Meatloaf int64 `json:"meatloaf"`
	Jowl     int64 `json:"jowl"`
	Enim     int64 `json:"enim"`
	Bresaola int64 `json:"bresaola"`
}

func ContWords() (BeefModels, error) {

	var beefModels BeefModels

	// load data
	text, err := loadText()
	if err != nil {
		utility.LogError.Println("err load text : ", err)
		return beefModels, err
	}

	// แปลง struct เป็น JSON
	jsonData, err := json.Marshal(beefModels)
	if err != nil {
		utility.LogError.Println("Error Marshal JSON:", err)
		return beefModels, err
	}

	// แปลง JSON เป็น map[string]interface{}
	var beefDataMap map[string]interface{}
	if err := json.Unmarshal(jsonData, &beefDataMap); err != nil {
		utility.LogError.Println("Error Unmarshal JSON:", err)
		return beefModels, err
	}

	// นับจำนวนครั้งที่ชื่อของ key ปรากฏในข้อความ
	for key := range beefDataMap {
		cleanedWord := strings.Trim(text, ",. ")
		count := strings.Count(strings.ToLower(cleanedWord), key)
		beefDataMap[key] = count
	}

	// แปลง map[string]interface{} เป็น JSON
	beefJson, err := json.Marshal(beefDataMap)
	if err != nil {
		utility.LogError.Println(err)
		return beefModels, err
	}

	// var result MyStruct
	err = json.Unmarshal(beefJson, &beefModels)
	if err != nil {
		return beefModels, err
	}

	return beefModels, nil
}

func loadText() (string, error) {
	// URL ที่ต้องการเรียกใช้
	url := "https://baconipsum.com/api/?type=meat-and-filler&paras=99&format=text"

	// ทำ HTTP GET request
	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error making GET request:", err)
		return "", err
	}
	defer response.Body.Close()

	// ตรวจสอบ HTTP status code
	if response.StatusCode != http.StatusOK {
		fmt.Printf("Unexpected status code: %d\n", response.StatusCode)
		return "", nil
	}

	// อ่านข้อมูลจาก response body
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading response body:", err)
		return "", err
	}

	return string(body), nil

}
