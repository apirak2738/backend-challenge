package resource

import "fmt"

func ConvertLRString(input string) string {
	result := ""
	sum := 0

	for i := 0; i < len(input); i += 2 {

		leftDigit := int(input[i] - '0')
		rightDigit := int(input[i+1] - '0')

		if leftDigit > rightDigit {
			result += "L"
		} else if rightDigit > leftDigit {
			result += "R"
		} else {
			result += "="
		}

		sum += leftDigit + rightDigit
		break
	}

	fmt.Println("Sum of digits: ", sum)
	return result
}
