package resource

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
)

func FindMaxSum() int {

	input := loadFile()

	rows := len(input)

	for i := rows - 2; i >= 0; i-- {
		for j := 0; j < len(input[i]); j++ {
			input[i][j] += max(input[i+1][j], input[i+1][j+1])
		}
	}
	return input[0][0]
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func loadFile() [][]int {
	fileContent, err := ioutil.ReadFile("./files/hard.json")
	if err != nil {
		log.Fatal("Error when opening file: ", err)
	}

	var response [][]int

	// แปลง JSON ในไฟล์เป็น slice ของ slice ของ int
	err = json.Unmarshal(fileContent, &response)
	if err != nil {
		fmt.Println("ไม่สามารถแปลง JSON ได้:", err)
		return nil
	}

	return response
}
